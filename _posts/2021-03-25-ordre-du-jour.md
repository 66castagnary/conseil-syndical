---
layout: default
title: "points à l'ordre du jour"
slug: ordre-du-jour
base: '../../../'
---
Nicolas et al.,

Il me semble que Monsieur Raeth relève un point important: il faut être certain que l'ordre du jour soit accepté par la majorité;
il me semble aussi que c'est la tache du conseil syndical de garantir cela, n'est pas ?

Ne serait-il pas nécessaire que le conseil syndical A+B atteigne une certaine unanimité sur les points de l'ordre du jour
Nous sommes 12 au conseil et j'aimerais voir l'approbation, est ce beaucoup demander ?
Peut être ce serait bien de collecter au moins un "commentaire" de chacun des membres du conseil syndical
ici présents dans la liste en "CC", ainsi que l'avis de Madame Azeb qui n'a pas d'email.

Donc par cette note, je m'adresse a l'ensemble du conseil syndical afin que chacun se prononce sur les 3 points
que Nicolas mentionne:

-  ravalement façade bât. B côté Charmilles
-  travaux de ravalement des murets de soutien des terres (côté dallage béton)
-  suppression du projet de placement des poubelles dans la cour


Si vous voulez pouvez le faire ici par email ou sur le groupe [telegram][1]

Pour ce qui est de mon avis: 

1. Parmi les travaux couteux qui sont nécessaires au bat B les travaux d'embellissements me semble moins prioritaire que des travaux de fonds : cave, structure bois qui pourrissent, écoulement eau, fuites, toitures, etc.ne serait il pas futile de ne pas réparer et maintenir un bâtiment qui vieillie et ne faire que des travaux "cosmétiques"  pour n'avoir qu'à la fin un château de cartes qui s'écroule. Donc pour le point 1 je propose que nous dressions la liste des travaux en "attentes" et nous voterons à l'AG leur priorité .

2. Pour le second point à prioi d'accord toutefois je demande à avoir plus d'information sur la nature des travaux,
ceci afin d'être sûr que nous protégeons les intérêts communs de la copropriété entière, j'espère que nous sommes tous d'accord la dessus.

3. Bien sur le point d'un aménagement d'un "point" recyclage carton/papier" dans la cour n'est pas aussi dramatique que "le placement des poubelles dans la cour",    il s'agit en effet de trouver une solution pour désengorger le "vrai" local poubelles afin d'assainir la copropriété et il en va de l'intérêt de tout le monde de le faire de manière constructive.  

Je pense personnellement que l'installation d'un point recyclage et d'une rampe facilitant la sortie du  container côté rue est une excellent idée. Bien sûr il n'est absolument pas question de mètre à "ciel ouvert" des poubelles dans la cour ce qui serait nuisible à tous.

Bien cordialement
<br>Monsieur Combes ([mgcombes@gmail.com][2])
<br>--&nbsp;
<br>phone: [+33 (0) 7 49 45 8957][3]

[1]: https://t.me/joinchat/CAFSManDsoZhNjdh
[2]: mailto:mgcombes@gmail.com
[3]: tel:+33749458957
